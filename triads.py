class Triads:
    """
    Store triads in an efficient manner.
    Each time a new group is added, calculate new triads.
    """

    def __init__(self):
        self.triads = []
        self.initial = []

    def append(self, group):
        if len(self.triads) == 0:
            self.initial = group
            self.triads = [group[i:i+3] for i in range(len(group) - (3-1))]
        else:
            group = self.initial[-2:] + group
            self.triads.extend([group[i:i+3] for i in range(len(group) - (3-1))])

if __name__ == '__main__':
    tri = Triads()
    tri.append([1, 2, 3, 4, 5, 6])
    print(tri.triads)
    tri.append([7, 8])
    print(tri.triads)
